<!--
  ~ SPDX-License-Identifier: Apache-2.0
  ~ Copyright IBM Corp 2023
-->

# Graphdb

The Graph Database is the backend storage of the MDM component. 
The metadata events from all MDM connectors are consolidated here, making it possible for other components to extract insights from the distributed system from a single pane of glass. 

## JSON schemas

The metadata stored in the Graph database and provided by the [MDM Connectors](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/metadata-manager-mdm/connectors) follows the JSON schema stored [here](./schemas).

## Installation
Set the environment variables accordingly:
```shellscript
export MDM_NAMESPACE=mdm
export MDM_CONTEXT=docker-desktop

kubectl --context=$MDM_CONTEXT create namespace  $MDM_NAMESPACE  
```
Add the Neo4j Helm repository:
```shellscript
helm repo add neo4j https://helm.neo4j.com/neo4j
```
Change the deployment [configuration file](./deployment/neo4j-helm.yaml) if required.

Install using Helm:
```shellscript
helm --kube-context=$MDM_CONTEXT install mdm-neo4j -n $MDM_NAMESPACE neo4j/neo4j-standalone -f ./deployment/neo4j-helm.yaml  
```

### Prometheus
Neo4j provides Prometheus endpoint on enterprise version only.
Enable Prometheus endpoint in `./deployment/neo4j-helm.yaml` with uncomment
the lines 
```yaml
  # metrics.prometheus.enabled: "true"
  # metrics.prometheus.endpoint: "localhost:2004"
```  

## License

The contents of this repository are licensed under the Apache-2.0 license.
The license full text can be found in [LICENSE](./LICENSE).

Neo4j's license can be found [here](https://neo4j.com/licensing/)

The insatllation description wiht using the `./deployment/neo4j-helm.yaml` install the community edition
```yaml
  edition: "community"
```
If a license exit, you can switch to the the enterprise version.
```yaml
  edition: "enterprise"
```

